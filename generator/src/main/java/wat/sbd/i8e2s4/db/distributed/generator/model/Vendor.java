package wat.sbd.i8e2s4.db.distributed.generator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vendor implements Persistable {
    private Long id;
    private String type;
    private String name;
    private String surname;
    private LocalDate joinDate;
}
