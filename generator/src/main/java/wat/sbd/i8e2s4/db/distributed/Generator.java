package wat.sbd.i8e2s4.db.distributed;

import org.apache.commons.cli.*;
import wat.sbd.i8e2s4.db.distributed.generator.DataGenerator;
import wat.sbd.i8e2s4.db.distributed.generator.mongo.MongoPersistence;
import wat.sbd.i8e2s4.db.distributed.generator.postgres.PostgresPersistence;

public class Generator {

    private static final Options cmdOptions = new Options()
            .addOption(Option.builder("t")
                    .longOpt("type")
                    .required()
                    .hasArg()
                    .desc("Type of database to save data, possible options: MONGO, POSTGRES")
                    .type(Type.class)
                    .build())
            .addOption(Option.builder("s")
                    .longOpt("size")
                    .required()
                    .hasArg()
                    .desc("Amount of transaction data to save, max 500 000")
                    .type(Integer.class)
                    .build())
            .addOption(Option.builder("tr")
                    .longOpt("truncate")
                    .desc("Choose this flag to delete all existing data")
                    .build());


    public static void main(String[] args) {
        CommandLine commandLine;
        try {
            commandLine = new DefaultParser().parse(cmdOptions, args);
        } catch (ParseException e) {
            new HelpFormatter().printHelp("java -jar <jar name> <options>", cmdOptions);
            return;
        }

        int size = Integer.valueOf(commandLine.getOptionValue("s"));
        if (size > 500000) {
            new HelpFormatter().printHelp("java -jar <jar name> <options>", cmdOptions);
            return;

        }
        DataGenerator generator;
        switch (Type.valueOf(commandLine.getOptionValue("t"))) {
            case MONGO:
                generator = new DataGenerator(new MongoPersistence());
                break;
            case POSTGRES:
                generator = new DataGenerator(new PostgresPersistence());
                break;
            default:
                new HelpFormatter().printHelp("java -jar <jar name> <options>", cmdOptions);
                return;

        }
        generator.generate(size, commandLine.hasOption("tr"));
    }

    private enum Type {
        MONGO,
        POSTGRES
    }
}
