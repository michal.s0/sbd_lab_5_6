package wat.sbd.i8e2s4.db.distributed.generator;

import lombok.RequiredArgsConstructor;
import wat.sbd.i8e2s4.db.distributed.generator.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@RequiredArgsConstructor
public class DataGenerator {
    private final DelegatingPersistence persistence;
    private final Random random = new Random();

    private List<Category> categories = new ArrayList<>();
    private List<Product> products = new ArrayList<>();
    private List<Rate> rates = new ArrayList<>();
    private List<Vendor> vendors = new ArrayList<>();

    public void generate(int size, boolean truncate) {
        if (truncate) {
            persistence.truncateData();
        }

        List<Category> generateCategories = generateCategories();
        categories.addAll(generateCategories);
        List<Product> generateProducts = generateProducts(calculateProductsNumber(size));
        products.addAll(generateProducts);
        List<Vendor> generateVendors = generateVendors(calculateVendorsNumber(size));
        vendors.addAll(generateVendors);
        List<Rate> generateRates = generateRates(size);
        rates.addAll(generateRates);

        List<Transaction> transactions = generateTransactions(size);

        persistence.save(generateCategories, Category.class);
        persistence.save(generateProducts, Product.class);
        persistence.save(generateRates, Rate.class);
        persistence.save(generateVendors, Vendor.class);

        persistence.save(transactions, Transaction.class);

    }

    private List<Category> generateCategories() {
        InputStream categoriesStream = getClass().getResourceAsStream("/generator/categories.csv");

        Map<String, String> categoryMap = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(categoriesStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] category = line.split(";");
                categoryMap.put(category[0], category[1]);
            }
        } catch (IOException e) {
            throw new RuntimeException("Error while parsing CSV file", e);
        }

        List<Category> categories = new ArrayList<>();

        categoryMap
                .forEach((key, value) -> {
                    Category category = new Category();
                    category.setName(key);
                    category.setDescription(value);

                    categories.add(category);
                });

        return categories;
    }

    private List<Product> generateProducts(int size) {
        List<Product> products = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            Product product = new Product();
            product.setCategory(randomElement(categories));
            product.setName(UUID.randomUUID().toString());
            product.setDescription(UUID.randomUUID().toString());
            product.setPrice(new BigDecimal(random.nextDouble()));

            products.add(product);
        }

        return products;
    }

    private List<Rate> generateRates(int size) {
        List<Rate> rates = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            Rate rate = new Rate();

            rate.setComment(UUID.randomUUID().toString());
            rate.setWeight(random.nextInt(10) + 1);

            rates.add(rate);
        }
        return rates;
    }

    private List<Vendor> generateVendors(int size) {
        List<Vendor> vendors = new ArrayList<>();

        InputStream namesStream = getClass().getResourceAsStream("/generator/names.csv");

        List<String> names = new ArrayList<>();
        List<String> surnames = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(namesStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] category = line.split(";");
                names.add(category[0]);
                surnames.add(category[1]);
            }
        } catch (IOException e) {
            throw new RuntimeException("Error while parsing CSV file", e);
        }

        for (int i = 0; i < size; i++) {
            Vendor vendor = new Vendor();
            vendor.setJoinDate(LocalDate.ofYearDay(random.nextInt(5) + 2015, random.nextInt(365) + 1));
            vendor.setName(randomElement(names));
            vendor.setSurname(randomElement(surnames));
            vendor.setType(UUID.randomUUID().toString());

            vendors.add(vendor);
        }

        return vendors;
    }

    private List<Transaction> generateTransactions(int size) {
        List<Transaction> transactions = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            Transaction transaction = new Transaction();

            transaction.setDate(LocalDate.ofYearDay(random.nextInt(10) + 2010, random.nextInt(365) + 1));
            transaction.setProduct(randomElement(products));
            transaction.setRate(randomElement(rates));
            transaction.setVendor(randomElement(vendors));

            transactions.add(transaction);
        }
        return transactions;
    }

    private int calculateProductsNumber(int size) {
        return size / 200 + 10;
    }

    private int calculateVendorsNumber(int size) {
        return size / 500 + 20;
    }


    private <T> T randomElement(List<T> list) {
        int size = list.size();

        return list.get(random.nextInt(size));
    }
}
