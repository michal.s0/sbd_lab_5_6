package wat.sbd.i8e2s4.db.distributed.generator;

import wat.sbd.i8e2s4.db.distributed.generator.model.*;

import java.util.List;

public interface DelegatingPersistence {
    String getDbType();

    void truncateData();

    <T extends Persistable> void save(List<T> data, Class clazz);

    List<Category> getCategories();
    List<Product> getProducts();
    List<Rate> getRates();
    List<Vendor> getVendors();
}
