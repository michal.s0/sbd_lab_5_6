package wat.sbd.i8e2s4.db.distributed.generator.postgres;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import wat.sbd.i8e2s4.db.distributed.generator.DelegatingPersistence;
import wat.sbd.i8e2s4.db.distributed.generator.model.*;
import wat.sbd.i8e2s4.db.distributed.generator.postgres.mybatis.config.PostgresSessionFactory;
import wat.sbd.i8e2s4.db.distributed.generator.postgres.mybatis.mappers.PostgresMapper;

import java.util.List;

public class PostgresPersistence implements DelegatingPersistence {
    private SqlSessionFactory sessionFactory = PostgresSessionFactory.getFactory();

    @Override
    public String getDbType() {
        return "POSTGRES";
    }

    @Override
    public void truncateData() {
        SqlSession sqlSession = sessionFactory.openSession();
        PostgresMapper mapper = sqlSession.getMapper(PostgresMapper.class);
        try {
            mapper.truncateTransactions();
            mapper.truncateProducts();
            mapper.truncateCategories();
            mapper.truncateRates();
            mapper.truncateVendors();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public <T extends Persistable> void save(List<T> data, Class clazz) {

        SqlSession sqlSession = sessionFactory.openSession();
        PostgresMapper mapper = sqlSession.getMapper(PostgresMapper.class);

        try {
            data.forEach(item -> {
                switch (clazz.getSimpleName()) {
                    case "Category":
                        save(() -> mapper.insertCategory((Category) item));
                        break;
                    case "Product":
                        save(() -> mapper.insertProduct((Product) item));
                        break;
                    case "Rate":
                        save(() -> mapper.insertRate((Rate) item));
                        break;
                    case "Vendor":
                        save(() -> mapper.insertVendor((Vendor) item));
                        break;
                    case "Transaction":
                        save(() -> mapper.insertTransaction((Transaction) item));
                        break;
                }
            });
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Category> getCategories() {
        SqlSession sqlSession = sessionFactory.openSession();
        PostgresMapper mapper = sqlSession.getMapper(PostgresMapper.class);

        List<Category> categories = mapper.findAllCategories();

        sqlSession.close();

        return categories;
    }

    @Override
    public List<Product> getProducts() {
        SqlSession sqlSession = sessionFactory.openSession();
        PostgresMapper mapper = sqlSession.getMapper(PostgresMapper.class);

        List<Product> products = mapper.findAllProducts();

        sqlSession.close();

        return products;
    }

    @Override
    public List<Rate> getRates() {
        SqlSession sqlSession = sessionFactory.openSession();
        PostgresMapper mapper = sqlSession.getMapper(PostgresMapper.class);

        List<Rate> rates = mapper.findAllRates();

        sqlSession.close();

        return rates;
    }

    @Override
    public List<Vendor> getVendors() {
        SqlSession sqlSession = sessionFactory.openSession();
        PostgresMapper mapper = sqlSession.getMapper(PostgresMapper.class);

        List<Vendor> vendors = mapper.findAllVendors();

        sqlSession.close();

        return vendors;
    }

    private void save(Runnable saving) {
        saving.run();
    }
}
