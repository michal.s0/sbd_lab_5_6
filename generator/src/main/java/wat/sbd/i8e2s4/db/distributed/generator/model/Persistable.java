package wat.sbd.i8e2s4.db.distributed.generator.model;

public interface Persistable {
    Long getId();
    void setId(Long id);
}
