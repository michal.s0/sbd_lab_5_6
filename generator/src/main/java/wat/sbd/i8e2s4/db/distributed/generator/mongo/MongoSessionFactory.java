package wat.sbd.i8e2s4.db.distributed.generator.mongo;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class MongoSessionFactory {
    private MongoDatabase database;

    private static MongoSessionFactory ourInstance = new MongoSessionFactory();

    public static MongoSessionFactory getInstance() {
        return ourInstance;
    }

    private MongoSessionFactory() {
        MongoClient mongoClient = MongoClients.create();
        database = mongoClient.getDatabase("mydb");
    }

    public MongoDatabase getDatabase() {
        return database;
    }
}
