package wat.sbd.i8e2s4.db.distributed.generator.postgres.mybatis.mappers;

import org.apache.ibatis.annotations.Param;
import wat.sbd.i8e2s4.db.distributed.generator.model.*;

import java.util.List;

public interface PostgresMapper {

    List<Category> findAllCategories();

    List<Rate> findAllRates();

    List<Product> findAllProducts();

    List<Vendor> findAllVendors();

    void truncateTransactions();

    void truncateProducts();

    void truncateCategories();

    void truncateRates();

    void truncateVendors();

    void insertCategory(@Param("category") Category category);

    void insertProduct(@Param("product") Product product);

    void insertRate(@Param("rate") Rate rate);

    void insertVendor(@Param("vendor") Vendor vendor);

    void insertTransaction(@Param("transaction") Transaction transaction);
}
