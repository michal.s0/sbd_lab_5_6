package wat.sbd.i8e2s4.db.distributed.generator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product implements Persistable {
    private Long id;
    private String name;
    private BigDecimal price;
    private String description;
    private Category category;
}
