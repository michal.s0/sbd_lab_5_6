package wat.sbd.i8e2s4.db.distributed.generator.postgres.mybatis.config;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class PostgresSessionFactory {
    private static SqlSessionFactory factory;

    private static void init() {
        InputStream resource;
        try {
            resource = Resources.getResourceAsStream("mybatis-config.xml");
        } catch (IOException e) {
            throw new RuntimeException("Error while initialization sql session factory");
        }

        factory = new SqlSessionFactoryBuilder().build(resource);
    }

    public static SqlSessionFactory getFactory(){
        if (factory == null) {
            init();
        }

        return factory;
    }
}
