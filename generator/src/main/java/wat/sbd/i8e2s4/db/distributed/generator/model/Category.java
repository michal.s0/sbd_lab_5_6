package wat.sbd.i8e2s4.db.distributed.generator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Category implements Persistable {
    private Long id;
    private String name;
    private String description;
}
