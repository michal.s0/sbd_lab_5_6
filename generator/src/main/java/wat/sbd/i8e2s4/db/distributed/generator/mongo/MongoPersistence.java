package wat.sbd.i8e2s4.db.distributed.generator.mongo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import wat.sbd.i8e2s4.db.distributed.generator.DelegatingPersistence;
import wat.sbd.i8e2s4.db.distributed.generator.model.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MongoPersistence implements DelegatingPersistence {

    private MongoCollection<Document> categoryCollection;
    private MongoCollection<Document> productCollection;
    private MongoCollection<Document> rateCollection;
    private MongoCollection<Document> vendorCollection;
    private MongoCollection<Document> transactionCollection;

    public MongoPersistence() {
        this.categoryCollection = MongoSessionFactory.
                getInstance().
                getDatabase().
                getCollection("category");
        this.productCollection = MongoSessionFactory.
                getInstance().
                getDatabase().
                getCollection("product");
        this.rateCollection = MongoSessionFactory.
                getInstance().
                getDatabase().
                getCollection("rate");
        this.vendorCollection = MongoSessionFactory.
                getInstance().
                getDatabase().
                getCollection("vendor");
        this.transactionCollection = MongoSessionFactory.
                getInstance().
                getDatabase().
                getCollection("transaction");
    }

    @Override
    public String getDbType() {
        return "MONGO";
    }

    @Override
    public void truncateData() {
        this.categoryCollection.drop();
        this.productCollection.drop();
        this.rateCollection.drop();
        this.vendorCollection.drop();
        this.transactionCollection.drop();
    }

    @Override
    public <T extends Persistable> void save(List<T> data, Class clazz) {
        generateIds(data, clazz);

        if (data.size() != 0) {
            switch (clazz.getSimpleName()) {
                case "Category":
                    this.categoryCollection.insertMany(this.getDocuments(data));
                    break;
                case "Product":
                    this.productCollection.insertMany(this.getDocuments(data));
                    break;
                case "Rate":
                    this.rateCollection.insertMany(this.getDocuments(data));
                    break;
                case "Vendor":
                    this.vendorCollection.insertMany(this.getDocuments(data));
                    break;
                case "Transaction":
                    this.transactionCollection.insertMany(this.getDocuments(data));
                    break;
            }
        }
    }

    private <T extends Persistable> void generateIds(List<T> data, Class<T> clazz) {
        List<T> list;
        switch (clazz.getSimpleName()) {
            case "Category":
                list = getObjects(categoryCollection, clazz);
                break;
            case "Product":
                list = getObjects(productCollection, clazz);
                break;
            case "Rate":
                list = getObjects(rateCollection, clazz);
                break;
            case "Vendor":
                list = getObjects(vendorCollection, clazz);
                break;
            case "Transaction":
                list = getObjects(transactionCollection, clazz);
                break;
            default:
                list = new ArrayList<>();
                break;
        }

        long biggest = list.stream()
                .sorted(Comparator.comparingLong(Persistable::getId).reversed())
                .map(Persistable::getId)
                .findFirst()
                .orElse(0L) + 1;

        for (int i = 0; i < data.size(); ++i) {
            T item = data.get(i);
            if (item.getId() == null) {
                item.setId(i + biggest);
            }
        }
    }

    @Override
    public List<Category> getCategories() {
        return getObjects(categoryCollection, Category.class);
    }

    @Override
    public List<Product> getProducts() {
        return getObjects(productCollection, Product.class);
    }

    @Override
    public List<Rate> getRates() {
        return getObjects(rateCollection, Rate.class);
    }

    @Override
    public List<Vendor> getVendors() {
        return getObjects(vendorCollection, Vendor.class);
    }

    public <T> List<T> getObjects(MongoCollection<Document> collection, Class<T> type) {
        List<T> list = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        for (Document doc : collection.find()) {
            try {
                T result = objectMapper.readValue(doc.toJson(), type);
                list.add(result);
            } catch (IOException e) {
                throw new RuntimeException("Error while parsing json from Mongo to object", e);
            }
        }
        return list;
    }

    private List<Document> getDocuments(List data) {
        List<Document> documents = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        data.forEach(item -> {
            try {
                String jsonStr = objectMapper.writeValueAsString(item);
                documents.add(Document.parse(jsonStr));
            } catch (JsonProcessingException e) {
                throw new RuntimeException("Error while parsing object to json", e);
            }
        });
        return documents;
    }
}
