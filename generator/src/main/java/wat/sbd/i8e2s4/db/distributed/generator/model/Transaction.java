package wat.sbd.i8e2s4.db.distributed.generator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction implements Persistable {
    private Long id;
    private Vendor vendor;
    private Rate rate;
    private Product product;
    private LocalDate date;
}
