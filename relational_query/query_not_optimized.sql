/* ZADANIE KTÓRE WYKONUJE ZAPYTANIE 

Zapytanie wyciągające top 100 użytkowników, którzy sprzedali najwięcej produktów, biorąc pod uwagę następujące warunki:
• sprzedawcy mają średnią ocen ze sprzedaży wszystkich swoich produktów w danej kategorii większą niż średnia ocen ze sprzedaży wszystkich produktów (również tych od innych sprzedawców) we wszystkich kategoriach;
• brane są pod uwagę tylko te kategorie, które mają więcej niż 10 ocen produktów należących do tych kategorii;
• sprzedawcy założyli konto najpóźniej w 2018 roku


*/

-- Zapytanie bez widoku zmaterializowanego
select 
    subquery.ID                                                                           as ID,
    subquery.IMIE || ' ' || subquery.NAZWISKO || ' (' || subquery.NAZWA_SPRZEDAWCY || ')' as SPRZEDAWCA,
    sum(subquery.LICZBA_PRODUKTOW)                                                        as LICZBA_SPRZEDANYCH_PRODUKTOW
from 
(
  select 
      s.ID_SPRZEDAWCY      as ID,
      s.IMIE               as IMIE,
      s.NAZWISKO           as NAZWISKO,
      s.NAZWA_SPRZEDAWCY   as NAZWA_SPRZEDAWCY,
      k.NAZWA              as NAZWA,
      k.ID_KATEGORII       AS ID_KATEGORII,
      avg(o.WARTOSC_OCENY) as SREDNIA_OCEN,
      count(p.NAZWA)       as LICZBA_PRODUKTOW
  from SPRZEDAWCA s
      join TRANSAKCJA t on s.ID_SPRZEDAWCY = t.ID_SPRZEDAWCY
      join PRODUKT p on t.ID_PRODUKTU = p.ID_PRODUKTU
      join KATEGORIA k on p.ID_KATEGORII = k.ID_KATEGORII
      join OCENA o on t.ID_OCENY = o.ID_OCENY
  where EXTRACT(YEAR FROM s.DATA_DOLACZENIA) <= '2018'
    and k.ID_KATEGORII in
      (
        select k1.ID_KATEGORII
        from TRANSAKCJA t1
            join PRODUKT p1 on t1.ID_PRODUKTU = p1.ID_PRODUKTU
            join KATEGORIA k1 on p1.ID_KATEGORII = k1.ID_KATEGORII
            join OCENA o1 on t1.ID_OCENY = o1.ID_OCENY
        group by k1.ID_KATEGORII
        having count(t1.ID_OCENY) > 10
      )
  group by s.ID_SPRZEDAWCY, s.IMIE, s.NAZWISKO, s.NAZWA_SPRZEDAWCY, k.NAZWA, k.ID_KATEGORII
  having avg(o.WARTOSC_OCENY) >
    (
      select avg(o2.WARTOSC_OCENY)
      from TRANSAKCJA t2
          join OCENA o2 on t2.ID_OCENY = o2.ID_OCENY
    )
) as subquery
group by subquery.ID, subquery.IMIE || ' ' || subquery.NAZWISKO || ' (' || subquery.NAZWA_SPRZEDAWCY || ')'
order by LICZBA_SPRZEDANYCH_PRODUKTOW DESC
limit 100;
