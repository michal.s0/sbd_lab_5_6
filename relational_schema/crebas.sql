/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     23.05.2019 21:03:30                          */
/*==============================================================*/


drop index KATEGORIA_PK;

drop table KATEGORIA;

drop index OCENA_PK;

drop table OCENA;

drop index Relationship_2_FK;

drop index PRODUKT_PK;

drop table PRODUKT;

drop index SPRZEDAWCA_PK;

drop table SPRZEDAWCA;

drop index Relationship_4_FK;

drop index Relationship_3_FK;

drop index Relationship_1_FK;

drop index TRANSAKCJA_PK;

drop table TRANSAKCJA;

/*==============================================================*/
/* Table: KATEGORIA                                             */
/*==============================================================*/
create table KATEGORIA (
   ID_KATEGORII         INT8                 not null,
   NAZWA                VARCHAR(255)         null,
   OPIS                 VARCHAR(2048)        null,
   constraint PK_KATEGORIA primary key (ID_KATEGORII)
);

/*==============================================================*/
/* Index: KATEGORIA_PK                                          */
/*==============================================================*/
create unique index KATEGORIA_PK on KATEGORIA (
ID_KATEGORII
);

/*==============================================================*/
/* Table: OCENA                                                 */
/*==============================================================*/
create table OCENA (
   ID_OCENY             INT8                 not null,
   WARTOSC_OCENY        INT4                 null,
   KOMENTARZ            VARCHAR(1024)        null,
   constraint PK_OCENA primary key (ID_OCENY)
);

/*==============================================================*/
/* Index: OCENA_PK                                              */
/*==============================================================*/
create unique index OCENA_PK on OCENA (
ID_OCENY
);

/*==============================================================*/
/* Table: PRODUKT                                               */
/*==============================================================*/
create table PRODUKT (
   ID_PRODUKTU          INT8                 not null,
   ID_KATEGORII         INT8                 null,
   NAZWA                VARCHAR(255)         null,
   CENA                 DECIMAL(10,2)        null,
   OPIS                 VARCHAR(2048)        null,
   constraint PK_PRODUKT primary key (ID_PRODUKTU)
);

/*==============================================================*/
/* Index: PRODUKT_PK                                            */
/*==============================================================*/
create unique index PRODUKT_PK on PRODUKT (
ID_PRODUKTU
);

/*==============================================================*/
/* Index: Relationship_2_FK                                     */
/*==============================================================*/
create  index Relationship_2_FK on PRODUKT (
ID_KATEGORII
);

/*==============================================================*/
/* Table: SPRZEDAWCA                                            */
/*==============================================================*/
create table SPRZEDAWCA (
   ID_SPRZEDAWCY        INT8                 not null,
   NAZWA_SPRZEDAWCY     VARCHAR(255)         null,
   IMIE                 VARCHAR(255)         null,
   NAZWISKO             VARCHAR(255)         null,
   DATA_DOLACZENIA      DATE                 null,
   constraint PK_SPRZEDAWCA primary key (ID_SPRZEDAWCY)
);

/*==============================================================*/
/* Index: SPRZEDAWCA_PK                                         */
/*==============================================================*/
create unique index SPRZEDAWCA_PK on SPRZEDAWCA (
ID_SPRZEDAWCY
);

/*==============================================================*/
/* Table: TRANSAKCJA                                            */
/*==============================================================*/
create table TRANSAKCJA (
   ID_TRANSAKCJI        INT8                 not null,
   ID_SPRZEDAWCY        INT8                 null,
   ID_OCENY             INT8                 null,
   ID_PRODUKTU          INT8                 null,
   CZAS_TRANSAKCJI      DATE                 null,
   constraint PK_TRANSAKCJA primary key (ID_TRANSAKCJI)
);

/*==============================================================*/
/* Index: TRANSAKCJA_PK                                         */
/*==============================================================*/
create unique index TRANSAKCJA_PK on TRANSAKCJA (
ID_TRANSAKCJI
);

/*==============================================================*/
/* Index: Relationship_1_FK                                     */
/*==============================================================*/
create  index Relationship_1_FK on TRANSAKCJA (
ID_SPRZEDAWCY
);

/*==============================================================*/
/* Index: Relationship_3_FK                                     */
/*==============================================================*/
create  index Relationship_3_FK on TRANSAKCJA (
ID_PRODUKTU
);

/*==============================================================*/
/* Index: Relationship_4_FK                                     */
/*==============================================================*/
create  index Relationship_4_FK on TRANSAKCJA (
ID_OCENY
);

alter table PRODUKT
   add constraint FK_PRODUKT_RELATIONS_KATEGORI foreign key (ID_KATEGORII)
      references KATEGORIA (ID_KATEGORII)
      on delete restrict on update restrict;

alter table TRANSAKCJA
   add constraint FK_TRANSAKC_RELATIONS_SPRZEDAW foreign key (ID_SPRZEDAWCY)
      references SPRZEDAWCA (ID_SPRZEDAWCY)
      on delete restrict on update restrict;

alter table TRANSAKCJA
   add constraint FK_TRANSAKC_RELATIONS_PRODUKT foreign key (ID_PRODUKTU)
      references PRODUKT (ID_PRODUKTU)
      on delete restrict on update restrict;

alter table TRANSAKCJA
   add constraint FK_TRANSAKC_RELATIONS_OCENA foreign key (ID_OCENY)
      references OCENA (ID_OCENY)
      on delete restrict on update restrict;


create sequence category_seq;
create sequence product_seq;
create sequence rate_seq;
create sequence vendor_seq;
create sequence transaction_seq;
